import RPi.GPIO as GPIO
import time
import http.client
import datetime

def update_lights(on):
	conn = http.client.HTTPConnection("10.0.0.17")

	if on:
		payload = "{\"on\":true}"
	else: 
		payload = "{\"on\":false}"

	headers = {
	    'content-type': "application/json",
	    'cache-control': "no-cache"
	    }

	conn.request("PUT", "/api/fTZqw1UoUIlEuPbNvHEi33ghmQopMRCHOFdYY9Gi/groups/1/action", payload, headers)

	res = conn.getresponse()
	data = res.read()

def no_movement_detected(time_stamp):
	return (datetime.datetime.now() - time_stamp).total_seconds() > 300

def main():
	should_handle=True #Flag to handle single signal from motion detector.
	is_turned_on=False #Flag to only update light if required.
	last_updated = datetime.datetime.now() #Timestamp for turning off timer.

	while True:
		input = GPIO.input(11)
		if input == 1:
			if should_handle == True:
				print("Motion detected!")
				
				if is_turned_on==False:
					print("Turning on...")
					update_lights(True)
					is_turned_on = True
				
				last_updated = datetime.datetime.now()
				should_handle = False
				time.sleep(4.0)
		else:
			should_handle=True
			if no_movement_detected(last_updated):
				print("No Motion detected in 5 minutes.")

				if is_turned_on == True:
					print("Turning off...")
					update_lights(False)
					is_turned_on = False

				last_updated = datetime.datetime.now()
				time.sleep(4.0)
		time.sleep(1.0)

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.IN)

main()

